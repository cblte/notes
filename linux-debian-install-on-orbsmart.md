# Debian Installation on the Orbsmart AW-08

## Preparation and Debian Installation

- Download a intel based debian image which should be put onto a USB stick
- Start the ORBSMART and hold down ESC for Bios or F7 for the bootmenu
- Install Debian via Text with the following settings:
  - english, english, German location
  - manual IP konfiguration 192.168.1.5
  - root user and your standard user name
  - do not install any graphical environment, only the SSH server and basic system utils

# Post Installation

- log in as root via MobaXTerm or iTerm
- upgrade sources and packages: \
  `apt update && apt upgrade`
- install intel microcode \
  `apt install intel-microcode`
- Install build-essential. At this step, I recommend installing the packages essential for compilation and installation of some programs. It is better to  install them early and forget. You can install them with this command: \
  `apt install build-essential dkms linux-headers-$(uname -r)` 
- We do not need so many restricted packages, but we might need rar \
  `apt install rar unrar`
- Install rsync \
  `apt install rsync`
- Install some missing networking tools \
`apt-get install net-tools`
- download and unpack btop - a better htop https://github.com/aristocratos/btop \
  if it is still in .tbz format, you can unpack it with `tar xjf <btop-filename>`
- (Install Log2Ram)


# Secure SSHD
On your remote machine generate a keypair. Either rsa or ed25519.

    ssh-keygen -t ed25519

after that do an ssh-copy-id to the server

    ssh-copy-id -i /c/Users/<TheUser>/.ssh/id_ed25519.pub -p 22 theuser@192.168.1.5

now login to the server and edit the `/etc/ssh/sshd_config`

Disable password authentication in sshd_config,

  PasswordAuthentication no

... which requires that your public ssh key is stored with the root in the file ~/root/.ssh/authorized_keys before. You can also disable root access if desired:

  PermitRootLogin no

In this case you have to make sure that another user can gain root privileges with sudo.

Additionally you can secure the machine with **fail2ban** to automatically detect attacking hackers and block their IP addresses, but this is not the topic here.


# Install Docker

Install Docker via manual and also follow some of the post installation steps
  
- https://docs.docker.com/engine/install/debian/
- https://docs.docker.com/engine/install/linux-postinstall/

Configure docker:
- use docker as non root user `sudo usermod -aG docker $USER` logout ofter executing the command
- configure docker to start at boot https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot
- confgure different logging driver https://docs.docker.com/engine/install/linux-postinstall/#configure-default-logging-driver

Config file
```
cat /etc/docker/daemon.json
{
  "dns": ["192.168.1.2"],
  "log-driver": "local",
    "log-opts": {
      "max-size": "10m"
    },
  "data-root": "/data/docker"
}
carsten@orbsmart:~$

``` 

# Install Portainer for a better visual feedback
- https://docs.portainer.io/v/ce-2.9/start/install/server/docker/linux




