## How To Compile writefreely so it runs on uberspace 7

Hi. This text is about the process on how to compile writefreely to make it run on uberspace7

I was able to have writefreely run on uberspace7 with the official release 0.12.0 from the github repository of write.as. But that version has a small issue with the generation of links for hashtags that start with a link. This issue was fixed in commit [#307](https://github.com/writeas/writefreely/pull/307). So I needed to update the binary. 

Unfortunataly I was not able to compile writefreely on uberspace because of not enough memory. The compile process gets killed while compiling a subcomponent of the [activity](https://github.com/writeas/activity) subproject. 

This is why I wrote this instructions set to compile writefreely on my own centos 7 machine which uses the same library and tool versions as the [uberspace 7](https://blog.uberspace.de/tag/uberspace7/) machines. 

All versions listed are the ones currently available as of writing this text

## Install Virtual Centos 7 machine

- download and install Virtual Box 
- create a virtual machine with 4GB RAM and 20GB harddisk
- go to  http://isoredirect.centos.org/centos/7/isos/x86_64/
- download a centos 7 minimal iso image (around 1GB)
- install centos on the virtual machine. choose default settings
- set root password and create the user as **admin**

## Preparing the system

We need to prepare the system after we have booted. First we will install a graphical environment for the browser. 

All commands need to be executed as `root`

### Graphical Environment (if you want)

If you want you can install a graphical environment. This is good for testing the writefreely installation in a good modern browser.

``` bash
yum install epel-release
yum update
yum groupinstall "X Window System"
yum groupinstall xfce
yum install firefox

systemctl set-default graphical.target
systemctl isolate graphical.target
``` 
Now we are in a graphical environment. Enter your login information and make sure to select XFCE as window manager if you dont want to use openbox

Open a terminal and install more software (change to root to do so if you dont want to execute everything with sudo).

### Install essential tools to download stuff and edit files

``` bash
yum install wget iftop htop nano vim git mc
```

### Install development chain

Now we install the development environment to get `go` running. 
We need the same tools as we have on uberspace 7 so we install a repository (dentos-release-scl-rh) and update from there. Install the GCLIB via `devtools-9` and the rest of the tool chain. 

``` bash
yum install centos-release-scl-rh
yum update
yum install devtools-9-libstdc++-devel
yum install gcc
yum install npm 
npm install less
npm install less-plugin-clean-css
```


### Install GOLANG

Now download and install golang `go1.15.2.linux-amd64.tar.gz` from the go website 

``` bash
wget https://golang.org/dl/go1.15.2.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.15.2.linux-adm64.tar.gz
```
add the path `/usr/local/go/bin/` to your $PATH environment

### Ready?

This should be all you need to compile writefreely on your new centos 7 machine

## Download and prepare writefreely

Regarding the setup for developers on the https://writefreely.org/docs/latest/developer/setup website it should be super easy. Actually it worked good after I had installed all the correct tools. Anyway, lets start. 

Execute the following commands (the first one, will take a while. Don't cancel it can take a while)

`go get -d github.com/writeas/writefreely/cmd/writefreely`

This will download the git repository and some additional tools that are needed to compile writefreely. After the command is finished, change into the go directory in your home folder and run the build commands

``` bash
cd ~/go/src/github.com/writeas/writefreely

make build # compile app
make install # start config, generate keys, setup database, compiles assets with LESS
make run # run the application
```

That should have done the work and you should now have a running installation of writefreely that you can test in your browser at the set url during the install phase. Press `CTRL+C` to stop the writefreely service. 

## Creating a release for 

To create a release for uberspace we can either modify the `Makefile` or zip up everything by hand. 

To zip up your package execute the following commands inside the writefreely directory which should still be `~/go/src/github.com/writeas/writefreely` 

``` bash
mkdir -p build/writefreely/
cp -r templates build/writefreely/
cp -r pages build/writefreely/
cp -r static build/writefreely/
mkdir build/writefreely/keys
make build
cp cmd/writefreely/writefreely build/writefreely/writefreely
tar -cvzf writefreely_$(git describe | cut -c 2)_linux_amd64.tar.gz -C build writefreely
```

The `-C` in the `tar` command will change into the `build` directory and put the `writefreely` into the archive.

Transfer the the writefreely archive over to uberspace. This can be done via a nullpointer instance

`curl -F'file@<writefreely_archive.tar.gz>' https://envs.sh`

This will return a download handle that can be used with wget on your uberspace server. 

## Change to uberspace

Login in to your uberspace and follow these steps:

- Stop the writefreely service: `supervisorctl stop writefreely`
- Rename the directory `mv writefreely writefreely.old`
- Download the file from the https://envs.sh server and extract it. 
- copy the database, keys and config file from the renamed directory

``` bash
cp writefreely.old/config.ini writefreely/
cp writefreely.old/writefreely.db writefreely/
cp -r writefreely.old/keys/ writefreely/
```

Now the only thing we need to do is to migrate the database to the current versions.

``` bash
cd writefreely
./writefreely db migrate
```

After migration we can start the service again

`supervisorctl start writefreely`
